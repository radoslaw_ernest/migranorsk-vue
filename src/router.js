import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Subpage from './views/Subpage.vue';
import Product from './views/Product.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/hjem',
      name: 'home',
      component: Home
    },
    {
      path: '/produkt/:product',
      name: 'product',
      props: true,
      component: Product
    },
    {
      path: '/produkter/alfa',
      name: 'product',
      props: true,
      component: Product
    },
    {
      path: '/:subpage',
      name: 'subpage',
      props: true,
      component: Subpage
    },
  ]
});
